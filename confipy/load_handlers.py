"""Module containting handling functions for **loader**."""


def overwrite(ctx, key, value):
    """Overwrite value when duplicity encountered.

    Parameters
    ----------
    ctx : dict
        Context dict.
    key : tuple of str
        Path.
    value
        Value.

    """
    ctx[key] = value
    return ctx


def skip(ctx, key, value):
    """Skip value already written to dict.

    Parameters
    ----------
    ctx : dict
        Context dict.
    key : tuple of str
        Path.
    value
        Value.

    """


def append(ctx, key, value):
    """Append duplicate key to list in context dict.

    Parameters
    ----------
    ctx : dict
        Context dict.
    key : tuple of str
        Path.
    value
        Value.

    """
    if not isinstance(ctx[key], list):
        ctx[key] = [ctx[key]]
    ctx[key].append(value)


def fail(ctx, key, value):
    """Raises exception on duplicity in context dict.

    Parameters
    ----------
    ctx : dict
        Context dict.
    key : str
        Path.
    value
        Value.

    Raises
    ------
    DuplicateKey
        Exception.

    """
    raise DuplicateKey(key, ctx[key], value)


class DuplicateKey(Exception):
    """Exception raised when duplicity is found in config."""

    def __init__(self, key, orig, new):
        super().__init__(
            f"Duplicity in context dict detected on key `{key}`,"
            f" where original value `{orig}` was to be ovewritten by `{new}`"
        )
