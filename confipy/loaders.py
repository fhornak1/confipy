"""Module containing various type of config loaders functions."""

import json as js
from configparser import ConfigParser

from .load_handlers import (
    fail,
    overwrite,
)
from .utils import (
    sanitize,
)


def json(path, on_duplicity=fail):
    """Create json loader.

    This method uses standard :func:`json.load` function.

    Parameters
    ----------
    path : str or os.PathLike
        Path to configuration.
    on_duplicity : callable, optional
        Duplicity resolver. Defaults to func:`fail`

    Returns
    -------
    dict
        Tree like configuration structure present in config file.

    """
    return _create_loader(js.load, path, on_duplicity=on_duplicity)


def ini(path, on_duplicity=fail):
    """Create ini config loader.

    Loader uses standard python :obj:`configparser.ConfigParser`.

    Parameters
    ----------
    path : str or os.PathLike
        Path to ini config file.
    on_duplicity : callback, optional
        Callback that is called when duplicity is encountered.
        Defaults to :func:`confipy.load_handlers.fail`.

    Returns
    -------
    Callable[[Dict[str, Any]], Dict[str, Any]]
        Lazy loader

    """
    def _loader(context):
        cfg = ConfigParser()
        with open(path, 'r') as file_handle:
            cfg.read_file(file_handle, path)

        for section_name, section in cfg.items():
            for key, value in section.items():
                pth = sanitize(section_name, key)
                if path in context:
                    context = on_duplicity(context, pth, value)
                else:
                    context[sanitize(section_name, key)] = value

        return context
    return _loader


try:
    import yaml as yml

    def yaml(path, on_duplicity=fail):
        """Create ``yaml`` loader.

        This function can be also used for loading ``json`` file format.
        This function uses ``PyYaml`` library.

        .. warning::

            This function will raise :class:`NotImplementedError`,
            when ``PyYaml`` is not installed.

        Parameters
        ----------
        path : str or os.PathLike
            Path to ``yaml`` configuration.

        """
        return _create_loader(yml.safe_load, path, on_duplicity=on_duplicity)

except ImportError:
    def yaml(path, on_duplicity=fail):
        """Create ``yaml`` loader.

        This function can be also used for loading ``json`` file format.
        This function uses ``PyYaml`` library.

        .. warning::

            This function will raise :class:`NotImplementedError`,
            when ``PyYaml`` is not installed.

        Parameters
        ----------
        path : str or os.PathLike
            Path to ``yaml`` configuration.

        """
        raise NotImplementedError(
            "This function requires `PyYaml` package to be installed."
        )

try:
    import toml as tml

    def toml(*paths):
        return _create_loader(tml.load, *paths)

except ImportError:
    def toml(*paths):
        raise NotImplementedError(
            "This function requires `toml` package to be installed."
        )


def _create_loader(load_fun, path, on_duplicity=fail):
    def _loader(context):
        def _assigner(ctx, key, value):
            if key in ctx:
                on_duplicity(ctx, key, value)
            else:
                ctx[key] = value

        with open(path, 'r') as file_handle:
            for key, value in flatten(load_fun(file_handle)):
                _assigner(context, key, value)

        return context

    return _loader


def merge(first, second):
    merged = dict(flatten(first))
    merged.update(dict(flatten(second)))
    return nest(merged.items())


def flatten(conf, prefix=()):
    """Flatten tree structure.

    Parameters
    ----------
    conf : Dict[str, Union[str, list, dict, int, bool, float, NoneType]
        Tree like configuration.
    prefix : tuple of str, optional
        Path prefix.

    Returns
    -------
    Dict[Seq[str], Any]
        Flattened configuration.

    """
    for key, val in conf.items():
        if isinstance(val, dict):
            for i_key, i_val in flatten(val, prefix + (key,)):
                yield i_key, i_val
        else:
            yield sanitize(*(*prefix, key)), val


NODE_VAL = '__value__'

def nest(flat):
    nested = {}
    for pth, value in flat:
        dct = nested
        for node in pth[:-1]:
            if node not in dct:
                dct[node] = {}
            elif not isinstance(dct[node], dict):
                dct[node] = {NODE_VAL: dct[node]}
            dct = dct[node]
        if isinstance(dct.get(pth[-1]), dict):
            dct[pth[-1]][NODE_VAL] = value
        else:
            dct[pth[-1]] = value
    return nested
