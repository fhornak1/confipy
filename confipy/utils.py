import re
from typing import (
    Callable,
    TypeVar,
    Union,
)


T = TypeVar('T')


_TRUE_VALUES = ('true', 't', 'yes', 'y', '1')
_FALSE_VALUES = ('false', 'f', 'no', 'n', '0')


def str2bool(value: str) -> bool:
    """Parse string value into boolean.

    Parses one of
    ``{'true', 't', 'yes', 'y', '1', 'false', 'f', 'no', 'n', '0'}``
    string value into appropriate boolean value.
    String value is striped and lower-cased before.

    Parameters
    ----------
    value : str
        Input with appropriate boolean string representation.

    Returns
    -------
    bool
        Boolean value of argument.

    Raises
    ------
    ValueError
        When sanitized string can not be matched by any of presented
        boolean representation values.

    """
    sanitized = value.strip().lower()
    if sanitized in _TRUE_VALUES:
        return True
    if sanitized in _FALSE_VALUES:
        return False
    raise ValueError(f'Unknown boolean value for `{value}`')


def identity(value: T) -> T:
    """Identity.

    Basic function that returns it's input.

    Parameters
    ----------
    inp : T
        Input.

    Returns
    -------
    T
        Input.

    """
    return value


def by_name(arg: Union[T, Callable[[], T]]) -> T:
    """Get value of provided arg.

    Convenience function that returns value of provided argument.
    It might be either callback producing output, or value.
    It's almost same as scala-s by_name argument ``arg: => T``.

    Parameters
    ----------
    arg : Union[T, Callable[[], T]]
        Argument from which it's value will be extracted.

    Returns
    -------
    T
        Extracted value.

    """
    if callable(arg):
        return arg()
    return arg


def sanitize(*key):
    return tuple(_sanitize_single(k) for k in key)


INVALID_CHARACTERS = re.compile(r"\W+")


def _sanitize_single(key):
    return INVALID_CHARACTERS.sub(
        '_',
        key.upper().strip()
    )
