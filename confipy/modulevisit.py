"""Module inspection tools for confipy."""
import inspect
from typing import (
    Any,
    List,
    Dict,
    Tuple,
    NamedTuple,
    Set
)
from types import (
    ModuleType,
)

_CONFIG_SUBMODULES = '__confipy_submodules__'


class ConfigValue(NamedTuple):
    """Configuration variable.

    Basic inspection element representing configuration variable stored
    inside moule, or module tree.

    """
    path: Tuple[str]
    name: str
    inspect: Tuple[type, Any]


def visit(module: ModuleType) -> List[ConfigValue]:
    """Build config tree from configuration module.

    Visits module and it's configuration submodules
    and collects all configuration variables.

    Parameters
    ----------
    module : ModuleType
        Root config module.

    Returns
    -------
    List[ConfigValue]
        Tree configuration structure.

    """
    return list(_walk(_MNode(module)))


def list_config_submodules(module: ModuleType) -> List[Tuple[str, ModuleType]]:
    """List all configuration submodules of module.

    Object is considerd as config submodule, iff it's a module
    object and also it's mentioned in ``_CONFIPY_SUBMODULES``
    tuple.

    Patameters
    ----------
    module : ModuleType
        Module for inspection.

    Returns
    -------
    List[Tuple[str, ModuleType]]
        List of pairs of config submodule name and itselve.

    """
    if _CONFIG_SUBMODULES not in module.__dict__:
        return []
    submods = module.__dict__[_CONFIG_SUBMODULES]
    return [(m, module.__dict__[m]) for m in submods
            if all([m in module.__dict__,
                    not m.startswith('_'),
                    inspect.ismodule(module.__dict__.get(m))])]


_ALLOWED_TYPES = (
    bool,
    int,
    float,
    str,
    list,
    dict,
    tuple,
    type(None),
)


def inspect_variables(module: ModuleType) -> Dict[str, Tuple[type, Any]]:
    """Inspect module variables or annotations.

    Parameters
    ----------
    module : ModuleType
        Module that should be inspected for config variable presence.

    Returns
    -------
    Dict[str, Tuple[type, Any]]
        Name-type pair of module global variables. Note that annotation
        types take precedence before what is currently stored in variable.

    """
    print('Globals of:', module)
    mod_vars = global_vars(module)
    print('Annotations of:', module)
    mod_vars.update(annotations(module))
    print(mod_vars)
    return mod_vars


def global_vars(module: ModuleType) -> Dict[str, Tuple[type, Any]]:
    """Filter global vars from module names.

    Parameters
    ----------
    module : ModuleType
        Module under inspection.

    Returns
    -------
    Dict[str, Tuple[type, object]]
        Dictionary of global name-type pairs.

    """
    return _list_elements(module.__dict__, type, identity)


def annotations(module: ModuleType) -> Dict[str, Tuple[type, Any]]:
    """Filter global var material from module annotations.

    Parameters
    ----------
    module : ModuleType
        Module that should be inspected.

    Returns
    -------
    Dict[str, Tuple[type, Any]]
        Dictionary of global name-type pairs.

    """
    return _list_elements(module.__annotations__, aliased_type, none)


def _list_elements(mod_elements, type_extract, value_extract):
    """Extracts global configuration variables out of the module.

    Parameters
    ----------
    mod_elements : Dict[str, object]
        Dictionary of module elements.
    type_extract : Callable[object, type]
        Type extractor function.
    value_extract : Callable[object, object]
        Value extractor function. This function is meant to take
        module variable and return itselve or None, if it's an
        annotation.

    Returns
    -------
    Dict[str, Tuple[type, object]]
        Mapping of variable name to pair of variable value and it's type.
        If annotation vas provided, value will be None.

    """
    return {k: (type_extract(v), value_extract(v))
            for k, v in mod_elements.items()
            if all([k == k.upper(),
                    not k.startswith('_'),
                    type_extract(v) in _ALLOWED_TYPES])}


def identity(x):
    return x


def aliased_type(x):
    return getattr(x, '__origin__', x)


def none(_x):
    return None


class _MNode:

    def __init__(self, module: ModuleType):
        self._visited = False
        self._module = module
        self._submodules = None
        self._config_variables = None

    @property
    def name(self):
        return self._module.__name__

    def visit(self):
        self._config_variables = inspect_variables(self._module)
        self._submodules = [
            (n, _MNode(m)) for n, m in list_config_submodules(self._module)
        ]

    @property
    def submodules(self):
        if not self._visited:
            self.visit()
        return self._submodules

    @property
    def variables(self):
        if not self._visited:
            self.visit()
        return self._config_variables


def _walk(node: _MNode, name='.', parent=()):
    print('Visitin mod: ', name, 'parent:', parent)
    for var_name, var_inspect in node.variables.items():
        yield ConfigValue(parent + (name,), var_name, var_inspect)
    for mod_name, mod in node.submodules:
        print('Visitin submodule', mod_name)
        for conf_value in _walk(mod, mod_name, parent=parent + (name, )):
            print('Yieldin value', conf_value)
            yield conf_value
