#!/usr/bin/env python
__version__ = '0.1.0'
from types import ModuleType
from typing import (
    Any,
    Dict,
)

from .load_handlers import (
    fail,
    overwrite
)

PATCH_STRICT = 'strict'
PATCH_LENIENT = 'lenient'

FLAT_TYPES = (bool, int, float, str, type(None))
FLAT_WITH_LIST_TYPES = FLAT_TYPES + (list,)

_ALLOWED_TYPES = (bool, int, float, str, list, type(None))
_PATCH = PATCH_STRICT
_PRECEDENCE = ('args', 'env', 'config')

# Works only with lenient patcher
_IGNORE_MISSING = False
_ALLOW_INT_TO_FLOAT_CONVERSION = False
_ALLOW_FLOAT_TO_INT_CONVERSION = False


def init(
        patch_type=PATCH_STRICT,
        allwoed_types=FLAT_TYPES,
        allow_int_to_float=False,
        allow_float_to_int=False,
        ignore_missing=False,
):
    """Initialize/configure confipy module.

    Parameters
    ----------
    patch_type
        str: Decindes if patching should be ``strict`` or ``lenient``.
    allowed_types
        Iterable[type]: List of types which could be patched.
    allow_int_to_float
        bool: Allows casting integer to floating point number,
              works only with lenient ``patch_type``.
    allow_float_to_int
        bool: Allows casting float to integer. Works only
              with *lenient* ``patch_type``.

    """
    global _ALLOWED_TYPES
    global _PATCH
    global _IGNORE_MISSING
    global _ALLOW_FLOAT_TO_INT_CONVERSION
    global _ALLOW_INT_TO_FLOAT_CONVERSION

    _ALLOWED_TYPES = allwoed_types
    _PATCH = patch_type
    _IGNORE_MISSING = ignore_missing
    _ALLOW_FLOAT_TO_INT_CONVERSION = allow_float_to_int
    _ALLOW_INT_TO_FLOAT_CONVERSION = allow_int_to_float



def load_from(*configs):
    pass


class Patcher:
    """Convenience object for patching ``config`` module in project."""

    def __init__(self,
                 allowed_types=FLAT_TYPES,
                 on_duplicity=overwrite,
                 on_load_error=fail):
        """Constructor.

        Parameters
        ----------
        allowed_types : Iterable[type]
            Determines which variable types could be patched in config.
        on_duplicity : Callable[[Dict[Tuple[str], Any], Dict[Tuple[str], Any]]
            Duplcity handler.
        on_load_error: Callable[[Exception]]
            Config file load error handler.

        """
        self._allowed_types = allowed_types

        self._loaders = []
        self._context = {}
        self._on_duplicate_key = on_duplicity
        self._on_load_error = on_load_error

    def append(self, loader):
        pass

    def overwrite(self, loader):
        pass

    def merge(self, loader, mode='overwrite'):
        pass

    def with_config(self, loader):
        """Append config file loader.

        Parameters
        ----------
        loader : callable
            Loader callback that can load particular config files.
            Loader callback takes context dict, ``on_duplicity``
            and ``on_load_error`` handlers, from which it is able
            to generate configuration.

        Returns
        -------
        Patcher
            Retruns itself.

        """
        self._loaders.append(loader)
        return self

    def load(self):
        for loader in self._loaders:
            self._context = loader(self._context,
                                   on_load_error=self._on_load_error,
                                   on_duplicity=self._on_duplicate_key)
        return self

    def patch(self, module):
        pass


def _patch_strict(module: ModuleType, conf: Dict[str, Any]):
    cfg_vars = inspect_variables(module)
    cfg_annotations = annotations(module)

    missing = cfg_annotations.keys() - conf.keys()

    if missing:
        raise Exception(
            f"Configuration does not contain following keys: `{missing}`,"
            " you should define them first, or use other non-strict mode"
        )

    for cfg_name, cfg_type in cfg_vars.items():
        value = conf[cfg_name]
        if not isinstance(value, cfg_type):
            raise Exception(
                f"Excpected type `{cfg_type}` is not"
                f" supper type of `{value}`"
            )
        setattr(module, cfg_name, value)


def flatten(conf: Dict[str, Any], sep='_', prefix=''):
    """Falaten configuration dict into non dict key-value pairs.

    Flattens config dict to flat structure containing only generated keys
    and their belonging values. Keys will be changed to standard upper case
    and ``-`` will be replaced with ``_`` character. For more information
    check :func:`config_key`.

    Parameters
    ----------
    conf : Dict[str, Any]
        Configuration dict.
    sep : str (optional)
        Character on which keys will be joined.
    prefix : str (optional)
        Substring that will be prepend to every generated key.

    Yields
    ------
    (str, Any)
        Key value pairs.

    """
    for k, val in conf.items():
        prefixed_key = join(prefix, k, separator=sep)
        if isinstance(val, dict):
            for i_k, i_v in flatten(val, sep, prefixed_key):
                yield config_key(i_k), i_v
        else:
            yield config_key(prefixed_key), val


def join(*keys, separator='_'):
    """Join provided keys into one using separator.

    If key is empty (i.e. ``bool(key) is False``), it will be filtered
    out.

    Parameters
    ----------
    *keys : Tuple[str]
        Key sequence.
    separator : str (optional)
        Separator used for joining provided keys.

    Returns
    -------
    str
        Joined key sequence, with filtered out empty keys.

    """
    return separator.join((e for e in keys if e))


def config_key(key: str) -> str:
    """Sanitize config key, into ``SCREAMING_SNAKE_CASE``.

    Parameters
    ----------
    key : str
        Key in various formats.

    Returns
    -------
    str
        Key in ``SCREAMING_SNAKE_CASE``.

    """
    return key.upper().replace('-', '_').replace('.', '_')


def inspect_variables(module: ModuleType) -> Dict[str, type]:
    """Inspect module variables or annotations.

    Parameters
    ----------
    module : ModuleType
        Module that should be inspected for config variable presence.

    Returns
    -------
    Dict[str, type]
        Name-type pair of module global variables. Note that annotation
        types take precedence before what is currently stored in variable.

    """
    mod_vars = global_vars(module)
    mod_vars.update(annotations(module))
    return mod_vars


def global_vars(module: ModuleType) -> Dict[str, type]:
    """Filter global vars from module names.

    Parameters
    ----------
    module : ModuleType
        Module under inspection.

    Returns
    -------
    Dict[str, type]
        Dictionary of global name-type pairs.

    """
    return _list_elements(module.__dict__, type)


def annotations(module: ModuleType) -> Dict[str, type]:
    """Filter global var material from module annotations.

    Parameters
    ----------
    module : ModuleType
        Module that should be inspected.

    Returns
    -------
    Dict[str, type]
        Dictionary of global name-type pairs.

    """
    return _list_elements(module.__annotations__, identity)


def _list_elements(mod_elems, type_extract):
    """Filter global variable class elements.

    Parameters
    ----------
    mod_elems : dict[str, object]
        Dict of module elements.
    type_extract : Callable[object, type]
        Extracts type of specified element.

    Returns
    -------
    dict[str, type]
        Name-type pairs of module elements that might be global variables.

    """
    return {k: type_extract(v) for k, v in mod_elems.items()
            if all([k == k.upper(),
                    not k.startswith('_'),
                    type_extract(v) in _ALLOWED_TYPES])}


def identity(x):
    return x
