from confipy.loaders import (
    ini,
    flatten,
    merge,
    nest,
    yaml,
)


def test_merge():
    first = {
        'a': 1,
        'b': {
            'c': {
                'd': 2,
                'e': 'abcd'
            },
            'f': 2,
        },
        'x': [1, 2, 3]
    }
    second = {
        'a': 2,
        'b': {
            'c': {
                'd': {
                    'f': 3
                },
                'e': 123
            },
            'z': 1
        }
    }

    expected = {
        'A': 2,
        'B': {
            'C': {
                'D': {
                    '__value__': 2,
                    'F': 3
                },
                'E': 123,
            },
            'F': 2,
            'Z': 1
        },
        'X': [1, 2, 3]
    }

    assert merge(first, second) == expected


def test_flatten():
    first = {
        'a': 1,
        'b': {
            'c': {
                'd': 2,
                'e': 'abcd'
            },
            'f': 2,
        },
        'x': [1, 2, 3]
    }
    expected = {
        ('A',): 1,
        ('B', 'C', 'D'): 2,
        ('B', 'C', 'E'): 'abcd',
        ('B', 'F'): 2,
        ('X',): [1, 2, 3]
    }
    assert dict(flatten(first)) == expected


def test_nest():
    flat = [
        (('a',), 1),
        (('b', 'c', 'd'), 2),
        (('b', 'c', 'e'), 'abcd'),
        (('b', 'f'), 2),
        (('x',), [1, 2, 3]),
        (('x', 'w'), 'uwxyz')
    ]
    expected = {
        'a': 1,
        'b': {
            'c': {
                'd': 2,
                'e': 'abcd'
            },
            'f': 2,
        },
        'x': {
            '__value__': [1, 2, 3],
            'w': 'uwxyz'
        }
    }
    assert nest(flat) == expected


def test_ini():
    loader = ini('tests/resources/test_1.ini')
    expected = {
        ('DEFAULT', 'REPEATEDVALUE'): '1',
        ('RESOURCE_1', 'NONREPEATEDVALUE'): '2',
        ('RESOURCE_1', 'FOO'): '"s3cr3t"',
        ('RESOURCE_1', 'REPEATEDVALUE'): '1',
        ('RESOURCE_2', 'OTHERVALUE'): '3',
        ('RESOURCE_2', 'BAR'): 'Other string value',
        ('RESOURCE_2', 'REPEATEDVALUE'): '1',
        ('RESOURCE_3', 'REPEATEDVALUE'): '"Foo"',
    }
    resource = loader({})

    assert expected == resource


def test_yaml():
    loader = yaml('tests/resources/test_2.yaml')
    expected = {
        ('POSTGRES', 'HOST'): "database.test",
        ('POSTGRES', 'PORT'): 5437,
        ('POSTGRES', 'DATABASE'): "test",
        ('POSTGRES', 'USER'): "test",
        ('POSTGRES', 'PASSWORD'): "s3cr3t",

        ('INFLUX', 'HOST'): "metrics.test",
        ('INFLUX', 'PORT'): 8087,
        ('INFLUX', 'USER'): "test",
        ('INFLUX', 'PASSWORD'): "s3cr3t",

        ('ELASTIC', 'NODES'): ['node1.es.test', 'node2.es.test'],

        ('WORKERS', 'LIMIT'): 15,
        ('WORKERS', 'MIN'): 3,

        ('DEBUG',): True

    }
    resource = loader({})
    assert expected == resource
