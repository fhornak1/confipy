from confipy.utils import (
    sanitize
)


def test_sanitize():
    assert sanitize('Fooo', 'bar', 'baz') == ('FOOO', 'BAR', 'BAZ')
