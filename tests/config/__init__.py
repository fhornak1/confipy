from . import submod1
from . import submod2


__confipy_submodules__ = ('submod1',)

FIRST_VARIABLE: str
SECOND_VARIABLE: float = 15
THRID_VARIABLE = [1, 2, 3, 4]
FOO = None
Bar = "abc"
_PROTECTED = "member"
BAZ: float = 3.1415

DICT_VAR: dict
