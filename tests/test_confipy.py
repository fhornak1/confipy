from confipy import (
    inspect_variables,
    annotations,
    global_vars,
    flatten,
    config_key,
    join
)

from . import config


def test_module_globals():
    expected = {
        'SECOND_VARIABLE': int,
        'THRID_VARIABLE': list,
        'FOO': type(None),
        'BAZ': float
    }
    assert expected == global_vars(config)


def test_module_annotations():
    expected = {
        'FIRST_VARIABLE': str,
        'SECOND_VARIABLE': float,
        'BAZ': float,
    }
    assert expected == annotations(config)


def test_list_config_keys():
    expected = {
        'FIRST_VARIABLE': str,
        'SECOND_VARIABLE': float,
        'THRID_VARIABLE': list,
        'FOO': type(None),
        'BAZ': float,
    }
    assert expected == inspect_variables(config)


def test_flatten():
    conf = {
        'postgre': {
            'host': 'localhost',
            'password': 's3cr3t',
            'port': 5432,
            'username': 'test',
            'database': 'test'
        },
        'list': [1, 2, 3, 4, 5],
        'global': {
            'foo': {
                'bar.baz': 123,
                'qux.Quux': 3.14
            },
            'show-help': True
        }
    }
    expected = {
        'POSTGRE_HOST': 'localhost',
        'POSTGRE_PASSWORD': 's3cr3t',
        'POSTGRE_PORT': 5432,
        'POSTGRE_USERNAME': 'test',
        'POSTGRE_DATABASE': 'test',
        'LIST': [1, 2, 3, 4, 5],
        'GLOBAL_FOO_BAR_BAZ': 123,
        'GLOBAL_FOO_QUX_QUUX': 3.14,
        'GLOBAL_SHOW_HELP': True
    }
    assert expected == dict(flatten(conf))


def test_to_global():
    assert 'POSTGRE' == config_key('postgre')
    assert 'POSTGRE_PASSWORD' == config_key('postgre-password')
    assert 'POSTGRE__HOST' == config_key('postgre--host')
    assert 'POSTGRE_PASSWORD' == config_key('postgre.password')
    assert 'GLOBAL_FOO_BAR_BAZ_QUUX' == config_key('global.foo.bar-baz_quux')

def test_join():
    assert join('foo') == 'foo'
    assert join('foo', 'bar') == 'foo_bar'
    assert join('foo', '') == 'foo'
    assert join('', 'foo') == 'foo'
    assert join('foo', '', 'bar') == 'foo_bar'

    # This is not expected behaviour, nor it should be used
    # Those tests just shows that I'm aware of this behaviour and
    # so should be you.
    assert join('foo', None) == 'foo'
    assert join('foo', 0) == 'foo'
