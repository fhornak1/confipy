from . import config

from confipy.modulevisit import (
    visit,
    ConfigValue
)


def test_visit():
    visited = visit(config)
    assert len(visited) == 7


