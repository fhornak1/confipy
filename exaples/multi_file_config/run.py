import config
import confipy


if __name__ == '__main__':
    confipy.load_from(
        ini('conf/influx.ini'),
        yaml('conf/config.yaml', confipy.skip),
        toml('conf/elastic.toml'),
        env('conf/elastic-hosts.env', confipy.append, split=','),
        env(confipy.overwrite)
    ).patch(config)
