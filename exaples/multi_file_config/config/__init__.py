from . import (
    database,
    elastic,
    influxdb as influx
)

__config_submodules__ = (
    'database',
    'elastic',
    'influx'
)


MAX_WORKERS = 1
